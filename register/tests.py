from django.test import TestCase,Client
from . import views
from django.urls import resolve
from django.contrib.auth.models import User

class Reguster(TestCase):

    def test_signup_url(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code,200)

    def test_login_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_signup_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response,'registration/signup.html')

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response,'registration/login.html')
