$(document).ready(function() {
	$(".button").mouseenter(function(){
		$(".button").css("color","black");
	});

	$(".button").mouseleave(function(){
		$(".button").css("color","white");
	});

	$(".bs").mouseenter(function(){
		$(".bs").css("color","blue");
	});

	$(".bs").mouseleave(function(){
		$(".bs").css("color","black");
	});

	$("#sbt-jumlah").click(function(){
		alert("Penduduk awal yang anda set akan berstatus negatif");
	});

	$(".sbh").click(function(e){
		e.preventDefault();
		var r =confirm($(this).attr('id') + " akan dikirim ke rumah sakit untuk penyembuhan");
		if (r==true){
			window.location.href= $(this).attr('href');
		}
		
	});

	$("#keyword").keyup(function(e){
		var key= this.value;
		$.ajax({
			url: "/simulasi/positif",
			type:"get",
			data: { name:key},
			success: function(data){
				$(".table").empty();
				$(".table").append(
                        `<tr style="background-color:  #6FCF97;font-weight: bold;">
                            <th>Nama </th>
                            <th>Usia</th>
                            <th>Alamat</th> 
                            <th>Jenis Kelamin</th> 
                            <th>Ubah Status</th> 
                        </tr>`);
				for(i=0;i<data.length;i++){
					$('.table').append(
						`<tr>
                            <td>${ data[i].nama_penduduk}</td>
                            <td>${ data[i].usia }</td>
                            <td>${ data[i].alamat }</td>
                            <td>${ data[i].jenis_kelamin }</td>
                            <td><a href="/simulasi/daftarpositif/${data[i].id}"  class="sbh" id='${ data[i].nama_penduduk }'><button  type="button" >Sembuhkan</button></a></td>
                        </tr>`)
				}
			}
		});
	});
});