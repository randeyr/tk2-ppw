from django.db import models

class PersentaseStatus(models.Model):
    positif = models.PositiveIntegerField()
    negatif = models.PositiveIntegerField()
        
class PersentaseJenisKelamin(models.Model):
    pria = models.PositiveIntegerField()
    perempuan = models.PositiveIntegerField()
        
class PersentaseUmur(models.Model):
    muda = models.PositiveIntegerField()
    tengah = models.PositiveIntegerField()
    dewasa = models.PositiveIntegerField()
    tua = models.PositiveIntegerField()