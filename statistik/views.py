from django.shortcuts import render, redirect
from simulasi.models import JumlahPendudukAwal
from simulasi.models import Penduduk
from .models import PersentaseStatus
from .models import PersentaseJenisKelamin
from .models import PersentaseUmur
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json

@login_required
def statCovid(request):

    # statistik awal
    persen_positif = 0
    persen_negatif = 0
    persen_pria = 0
    persen_perempuan = 0
    persen_muda = 0
    persen_dewasa = 0
    persen_tengah = 0
    persen_tua = 0

    # membuat objek untuk setiap model untuk menyimpan data
    PersentaseStatus.objects.create(positif=persen_positif, negatif=persen_negatif)
    PersentaseJenisKelamin.objects.create(pria=persen_pria, perempuan=persen_perempuan)
    PersentaseUmur.objects.create(muda=persen_muda, tengah=persen_tengah, dewasa=persen_dewasa, tua=persen_tua)

    # untuk mengakses objek yang sudah dibuat
    status = PersentaseStatus.objects.all()[0]
    gender = PersentaseJenisKelamin.objects.all()[0]
    umur = PersentaseUmur.objects.all()[0]

    jumlah_awal = JumlahPendudukAwal.objects.all().count()

    if JumlahPendudukAwal.objects.all().count() > 0 and Penduduk.objects.filter(status_covid='Positif').count() == 0:
        persen_negatif = 100

    elif Penduduk.objects.filter(status_covid='Positif').count() > 0:
        
        awal = JumlahPendudukAwal.objects.all()[0]
        total = awal.jumlah + Penduduk.objects.all().count()
        
        # statistik berdasarkan status covid
        jumlah_positif = Penduduk.objects.filter(status_covid='Positif').count()
        persen_positif = jumlah_positif * 100 // total
        persen_negatif = 100 - persen_positif

        # statistik berdasarkan jenis kelamin
        jumlah_pria = Penduduk.objects.filter(status_covid='Positif', jenis_kelamin='Laki-Laki').count()
        persen_pria = jumlah_pria * 100 // jumlah_positif
        persen_perempuan = 100 - persen_pria

        # statistik berdasarkan usia
        usiaMuda = 0
        usiaDewasa = 0
        usiaTengah = 0
        usiaTua = 0

        for penduduk in Penduduk.objects.filter(status_covid='Positif'):
            if int(penduduk.usia) <= 20:
                usiaMuda += 1

            elif int(penduduk.usia) > 20 and int(penduduk.usia) < 45:
                usiaDewasa += 1

            elif int(penduduk.usia) >= 45 and int(penduduk.usia) <= 65:
                usiaTengah += 1

            else:
                usiaTua +=1
        
        persen_muda = usiaMuda * 100 // jumlah_positif
        persen_dewasa = usiaDewasa * 100 // jumlah_positif
        persen_tengah = usiaTengah * 100 // jumlah_positif
        persen_tua = usiaTua * 100 // jumlah_positif

    status.positif = persen_positif
    status.negatif = persen_negatif
    status.save()

    gender.pria = persen_pria
    gender.perempuan = persen_perempuan
    gender.save()

    umur.muda = persen_muda
    umur.tengah = persen_tengah
    umur.dewasa = persen_dewasa
    umur.tua = persen_tua
    umur.save()

    response = {
        'status': status,
        'gender': gender,
        'umur': umur,
        'awal': jumlah_awal
    }
    return render(request, 'stats.html', response)

@login_required
def statCovid_ajax(request):

    data = dict()
    data['positif'] = PersentaseStatus.objects.all()[0].positif
    data['negatif'] = PersentaseStatus.objects.all()[0].negatif
    data['pria'] = PersentaseJenisKelamin.objects.all()[0].pria
    data['perempuan'] = PersentaseJenisKelamin.objects.all()[0].perempuan
    data['muda'] = PersentaseUmur.objects.all()[0].muda
    data['tengah'] = PersentaseUmur.objects.all()[0].tengah
    data['dewasa'] = PersentaseUmur.objects.all()[0].dewasa
    data['tua'] = PersentaseUmur.objects.all()[0].tua

    response = {
        'data': data,
    }
    return JsonResponse(response)

@login_required
def cobaLagi(request):

    JumlahPendudukAwal.objects.all().delete()
    Penduduk.objects.all().delete()
    PersentaseStatus.objects.all().delete()
    PersentaseJenisKelamin.objects.all().delete()
    PersentaseUmur.objects.all().delete()

    return redirect('/simulasi/jumlah')