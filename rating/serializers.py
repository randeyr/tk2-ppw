from rest_framework import serializers
from .models import Akun

class AkunSerializer(serializers.ModelSerializer):

	username = serializers.CharField(source='username.username',read_only=True)
	class Meta:
		model = Akun
		fields = '__all__'