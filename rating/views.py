from django.shortcuts import render,get_object_or_404
from .models import Akun
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib import messages
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import AkunSerializer
# Create your views here.

@login_required
def halaman_form(request):

    return render(request,'halaman_form.html')

@login_required
def feedback_form(request):
    args = {}
    try:
        rating = request.POST.get('inlineRadioOptions','5')
        user_rating = Akun(rating=rating)
        user_rating.username = request.user
        user_rating.save()
        tampilan = "Rating Anda Berhasil Tersimpan"
        messages.success(request,tampilan)
    except IntegrityError as e: 
        tampilan = "Rating Dengan Username " + request.user.username + " Sudah Tersedia!"
        messages.warning(request,tampilan)

    return render(request,'halaman_form.html',args)


def halaman_rating(request):
    info_rating = Akun.objects.all()
    rating = {
        "info_rating" : info_rating
    }
    return render(request,'halaman_rating.html',rating)

def edit_rating(request):
    
    rating = request.POST.get('inlineRadioOptions','5')
    Akun.objects.filter(username=request.user).update(rating=rating)
    info_rating = Akun.objects.all()
    rating = {
        "info_rating" : info_rating
    }
    return render(request,'halaman_rating.html',rating)

class AkunList(APIView):
    def get(self,request):
        obj_akun = Akun.objects.all()
        serializer = AkunSerializer(obj_akun,many=True)
        return Response(serializer.data)

    def post(self):
        pass