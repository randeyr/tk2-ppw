from django.test import TestCase,Client
from .models import Akun
from .views import halaman_form,feedback_form,halaman_rating
from django.urls import resolve
from django.contrib.auth.models import User
import json

# Create your tests here.

class TestRating(TestCase):
    # Test urls
    def test_halaman_rating_exist(self):
        response = Client().get('/rating/')
        self.assertEqual(response.status_code,200)

    def test_halaman_form_exist(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = Client().get('/rating/ratingwebkami/')
        self.assertEqual(response.status_code,302)
    
    def test_feedback_form_exist(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = Client().get('/rating/berhasil/')
        self.assertEqual(response.status_code,302)
    
    # # Test views
    def test_func_halaman_form(self):
        find = resolve('/rating/')
        self.assertEqual(find.func,halaman_rating)
    
    def test_func_feedback_form(self):
        find = resolve('/rating/berhasil/')
        self.assertEqual(find.func,feedback_form)
    
    def test_func_halaman_rating(self):
        find = resolve('/rating/ratingwebkami/')
        self.assertEqual(find.func,halaman_form)

    # # Test models
    def test_model_akun(self):
        self.user = User.objects.create_user(username='test_model',password='12345')
        info_rating = Akun.objects.create(username=self.user,rating='5')
        test_model = Akun.objects.all().count()
        self.assertEqual(test_model,1)
    
    # # Test template

    def test_template_list_rating(self):
        response = Client().get('/rating/')
        self.assertTemplateUsed(response,'halaman_rating.html')
    

    def test_template_feedback_form(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = self.client.get('/rating/berhasil/')
        self.assertTemplateUsed(response,'halaman_form.html')
        
 
    def test_template_form(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = self.client.get('/rating/ratingwebkami/')
        self.assertTemplateUsed(response,'halaman_form.html')
    
    def test_content_form(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = self.client.get('/rating/ratingwebkami/')
        content = response.content.decode('utf8')
        self.assertIn("Berikan Rating Pada Web Kami",content)
        self.assertIn("Submit",content)
    
    def test_content_feedback_form(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = self.client.get('/rating/berhasil/')
        content = response.content.decode('utf8')
        self.assertIn("Rating Anda Berhasil Tersimpan",content)
    
    def test_content_list_rating(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        login = self.client.login(username='test_user',password='12345')
        response = self.client.post('/rating/berhasil/',{ Akun.rating : '5'})
        response_list = Client().get('/rating/')
        isi_post = response_list.content.decode('utf8')
        self.assertIn("test_user",isi_post)
        self.assertIn("5",isi_post)

    def test_login_url(self):
        with self.settings(LOGIN_URL='/login/'):
            response = self.client.get('/rating/ratingwebkami/')
            self.assertRedirects(response,'/login/?next=/rating/ratingwebkami/')

    def test_json_in_rating(self):
        self.user = User.objects.create_user(username='test_user',password='12345')
        response_post = self.client.post('/rating/berhasil/',{Akun.rating:'5'})
        response = Client().get('/rating/akun/?format=json')
        response2 = Client().get('/rating/')
        isi_post = response2.content.decode('utf8')
        data_from_rating = json.loads(response.content)
        test_data = []
        self.assertIn("5",isi_post)
        self.assertListEqual(test_data,data_from_rating)

    def test_json_url(self):
        response = Client().get('/rating/akun/')
        self.assertEqual(response.status_code,200)
       

