from django.urls import path
from . import views

app_name = 'rating'

urlpatterns = [
    path('',views.halaman_rating, name='halaman_rating'),
    path('ratingwebkami/',views.halaman_form,name='halaman_form'),
    path('berhasil/',views.feedback_form,name='feedback_form'),
    path('editrating/',views.edit_rating,name='edit_rating'),
]

