from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth import login, get_user

class indexTest(TestCase):
    def test_index_urls_is_exist(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_index_templates(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_index_using_func(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        found = resolve('/')
        self.assertEqual(found.func, index)

    
