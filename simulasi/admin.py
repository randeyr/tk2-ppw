from django.contrib import admin
from .models import JumlahPendudukAwal, Penduduk

# Register your models here.
admin.site.register(JumlahPendudukAwal)
admin.site.register(Penduduk)
