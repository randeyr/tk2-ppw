from django.shortcuts import render,get_object_or_404
from .forms import FormPenduduk, FormJumlahPenduduk
from .models import Penduduk, JumlahPendudukAwal
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import PendudukSerializer


# Create your views here.
@login_required
def jumlah(request):
	formjumlah=FormJumlahPenduduk()
	return render(request, 'jumlah.html',{'formjumlah':formjumlah})

@login_required
def form(request):
	form=FormPenduduk()
	return render(request, 'form.html',{'form':form})

@login_required
def savejumlah(request):
	args = {}
	args_warning = {}
	if request.method=='POST':
		formjumlah=FormJumlahPenduduk(request.POST)
		if formjumlah.is_valid():
			if(JumlahPendudukAwal.objects.all().count()<1):
				formjumlah.save()
				return HttpResponseRedirect('/simulasi/form')


			else:
				formjumlah = FormJumlahPenduduk()
				warn= "Maaf Penduduk Awal Hanya Bisa Dimasukan Sekali Per Simulasi"
				args['warn']=warn
				return render(request,'jumlah.html',{'warn':warn,'formjumlah':formjumlah})

			
		else:
			formjumlah = FormJumlahPenduduk()
			warning = "Value tidak sesuai"
			args_warning['warning'] = warning
			return render(request,'jumlah.html',{'warning':warning,'formjumlah':formjumlah})

	

@login_required
def savependuduk(request):
	args= {}
	if request.method=='POST':
		form = FormPenduduk(request.POST)
		if form.is_valid():
			data = Penduduk()
			data.nama_penduduk = form.cleaned_data['nama_penduduk']
			data.alamat = form.cleaned_data['alamat']
			data.usia= form.cleaned_data['usia']
			data.jenis_kelamin = form.cleaned_data['jenis_kelamin']
			data.status_covid = form.cleaned_data['status_covid']
			data.save()
			berhasil= "Berhasil Menambahkan Warga!"
			args['berhasil']=berhasil
			
			return render(request, 'form.html', {'berhasil':berhasil,'form' : form})
	

    
@login_required
def sembuhkan(request,id):
    Penduduk.objects.filter(id=id).delete()
    penduduk = Penduduk.objects.all().filter(status_covid='Positif')
    return render(request, 'daftarpositif.html', {'positif':penduduk})

@login_required
def daftarpositif(request):
	positif = Penduduk.objects.all().filter(status_covid='Positif')
	html = 'daftarpositif.html'
	return render(request, html, {'positif':positif})

class PendudukList(APIView):
	def get(self,request):
		positif = Penduduk.objects.all().filter(status_covid='Positif')
		name = self.request.query_params.get('name', None)
		if name is not None:
			positif= positif.filter(nama_penduduk__contains=name)
		serializer = PendudukSerializer(positif,many=True)
		return Response(serializer.data)

	def post(self):
		pass
