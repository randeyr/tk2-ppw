from rest_framework import serializers
from .models import Penduduk

class PendudukSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Penduduk
		fields = '__all__'