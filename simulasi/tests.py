
from django.test import TestCase,Client
from .models import JumlahPendudukAwal, Penduduk
from .views import jumlah,form,savejumlah,savependuduk,sembuhkan,daftarpositif
from django.urls import resolve
from .forms import FormJumlahPenduduk
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import get_user
# Create your tests here.

class TestSimulasi(TestCase):
	# test urls
    def test_jumlah_exist(self):
        user = get_user(self.client)
        response = Client().get('/simulasi/jumlah/')
        self.assertEqual(response.status_code,302)
        
        self.user = User.objects.create_user(username='garryhanuga', password='mantap99')
        login = self.client.login(username='garryhanuga', password='mantap99')
        response = self.client.get('/simulasi/jumlah/')
        self.assertEqual(response.status_code,200)

    def test_daftarpositif_exist(self):
        user = get_user(self.client)
        response = Client().get('/simulasi/daftarpositif/')
        self.assertEqual(response.status_code,302)
        
        self.user = User.objects.create_user(username='garryhanuga', password='mantap99')
        login = self.client.login(username='garryhanuga', password='mantap99')
        response = self.client.get('/simulasi/daftarpositif/')
        self.assertEqual(response.status_code,200)

    def test_form_penduduk_exist(self):
        user = get_user(self.client)
        response = Client().get('/simulasi/form/')
        self.assertEqual(response.status_code,302)
        
        self.user = User.objects.create_user(username='garryhanuga', password='mantap99')
        login = self.client.login(username='garryhanuga', password='mantap99')
        response = self.client.get('/simulasi/form/')
        self.assertEqual(response.status_code,200)

    # #test model
    def test_model_jumlah(self):
    	JumlahPendudukAwal.objects.create(jumlah=15)
    	jumlah_objek=JumlahPendudukAwal.objects.all().count()
    	self.assertEquals(jumlah_objek,1)

    def test_model_penduduk_gagal(self):
    	Penduduk.objects.create(nama_penduduk="Asep",usia="35", alamat="x", jenis_kelamin="Laki-Laki", status_covid="Negatif")
    	jumlah_daftar=Penduduk.objects.all().filter(status_covid='Positif').count()
    	self.assertEquals(jumlah_daftar,0)

    def test_model_penduduk_berhasil(self):
    	Penduduk.objects.create(nama_penduduk="Asep",usia="35", alamat="x", jenis_kelamin="Laki-Laki", status_covid="Positif")
    	jumlah_daftar=Penduduk.objects.all().filter(status_covid='Positif').count()
    	self.assertEquals(jumlah_daftar,1)

    def test_hapus_model_penduduk(self):
        Asep=Penduduk.objects.create(nama_penduduk="Asep",usia="35", alamat="x", jenis_kelamin="Laki-Laki", status_covid="Positif")
        Penduduk.objects.all().filter(id= Asep.id).delete()
        jumlah_daftar=Penduduk.objects.all().filter(status_covid='Positif').count()
        self.assertEquals(jumlah_daftar,0)


    #test feedback
    def test_feedback_jumlah(self):
        JumlahPendudukAwal.objects.create(jumlah=15)
        self.user = User.objects.create_user(username='garryhanuga', password='mantap99')
        login = self.client.login(username='garryhanuga', password='mantap99')
        response = self.client.post('/simulasi/jumlah/savejumlah',{"jumlah" : 15})
        content = response.content.decode('utf8')
        self.assertIn("Maaf Penduduk Awal Hanya Bisa Dimasukan Sekali",content)

    def test_feedback_jumlah_2(self):
        self.user = User.objects.create_user(username='garryhanuga', password='mantap99')
        login = self.client.login(username='garryhanuga', password='mantap99')
        response = self.client.post('/simulasi/jumlah/savejumlah',{"jumlah" : -2})
        content = response.content.decode('utf8')
        self.assertIn("Value tidak sesuai",content)

    def test_feedback_form_penduduk(self):
        self.user = User.objects.create_user(username='garryhanuga', password='mantap99')
        login = self.client.login(username='garryhanuga', password='mantap99')
        response = self.client.post('/simulasi/form/savependuduk' ,{'nama_penduduk' : 'Asep', 'usia' : '55', 'jenis_kelamin': 'Laki-Laki', 'status_covid': 'Positif', 'alamat': 'x'})
        content = response.content.decode('utf8')
        self.assertIn("Berhasil Menambahkan Warga!",content)



    
    

    
    
