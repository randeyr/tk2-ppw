from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth import get_user
from django.contrib.auth.models import User
from .views import listfeedback, tambah, saveform, likeWithJsonData, dislikeWithJsonData
from .models import posts

class feedbackTest(TestCase):

    
    def test_feedback_urls_is_exist(self):
        user = get_user(self.client)
        response = Client().get("/feedback/")
        self.assertEqual(response.status_code,302)
        
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get("/feedback/")
        self.assertEqual(response.status_code,200)

    
    def test_feedback_templates_is_exist(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get("/feedback/")
        self.assertTemplateUsed(response, 'feedback.html')
  
    def test_feedback_using_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, listfeedback)

    def test_tambah_urls_is_exist(self):
        user = get_user(self.client)
        response = Client().get('/feedback/tambah/')
        self.assertEqual(response.status_code,302)

        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/feedback/tambah/')
        self.assertEqual(response.status_code,200)

    def test_tambah_templates_is_exist(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/feedback/tambah/')
        self.assertTemplateUsed(response, 'tambah.html')
    
    def test_tambah_using_func(self):
        found = resolve('/feedback/tambah/')
        self.assertEqual(found.func, tambah)

    def test_saveform_url_is_exist(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.post('/feedback/saveform/')
        self.assertEqual(response.status_code, 302)

    def test_can_save_POST_request(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.post('/feedback/saveform/', data={ 
            'pesan' : 'b','img':'/static/1.png'})
        count_all = posts.objects.all().count()
        self.assertEqual(count_all, 1)

        self.assertEqual(response.status_code,302)
        response = self.client.get('/feedback/tambah/')

    def test_model_can_create_new_activity(self):
        new_feeds = posts.objects.create(nama="a",pesan="b",img="1",num=2, like = 0, likedBy = "")
        new_feeds.save()
        count_all = posts.objects.all().count()
        self.assertEqual(count_all, 1)

    def test_like_urls_is_exist(self):
        new_feeds = posts.objects.create(nama="a",pesan="b",img="1",num=2, like = 0, likedBy = "")
        new_feeds.save()

        response = Client().get('/feedback/like/?q=0')
        self.assertEqual(response.status_code, 200)
    

    def test_dislike_urls_is_exist(self):
        new_feeds = posts.objects.create(nama="a",pesan="b",img="1",num=2, like = 0, likedBy = "")
        new_feeds.save()
        
        response = Client().get('/feedback/dislike/?q=0')
        self.assertEqual(response.status_code, 200)

    def test_like_using_func(self):
        found = resolve('/feedback/like/')
        self.assertEqual(found.func, likeWithJsonData)
        
    def test_dislike_using_func(self):
        found = resolve('/feedback/dislike/')
        self.assertEqual(found.func, dislikeWithJsonData)


    