from django.shortcuts import render,get_object_or_404
from django.http import HttpResponseRedirect,JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import posts
from .forms import input_pesan
from .serializers import feedbackSerializer
from django.contrib.auth.decorators import login_required
import requests, json

# Create your views here.
@login_required
def listfeedback(request):
    feedbacks = posts.objects.all()
    status = []
    for p in range(len(feedbacks)):
        if request.user.username in feedbacks[p].likedBy.split("-"):
            status.append(p-1)
    response={
        'feedbacks' : feedbacks,
        'status' : status,
    }
    return render(request, 'feedback.html', response)

@login_required
def tambah(request):
    response={
        'form2' : input_pesan,
    }
    return render(request, 'tambah.html', response)

@login_required
def saveform(request):
    if request.method == 'POST':
        form = input_pesan(request.POST)
        pimg = request.POST.get('img-picker')
        if form.is_valid():
            name = request.user.username
            pesen = form.cleaned_data['pesan']
            simg = switchimg(pimg)
            nom = len(posts.objects.all()) + 1
            feedbacks = posts(nama = name, pesan = pesen, img=simg, num = nom, like = 0, likedBy = "")
            feedbacks.save()
            return HttpResponseRedirect('/feedback/')
        else:
            return HttpResponseRedirect('/feedback/')




def switchimg(arg):
    switcher = {
        '1': "/static/1.png",
        '2': "/static/2.png",
        '3': "/static/3.png",
        '4': "/static/4.png",
        '5': "/static/5.png",
        '6': "/static/6.png",
        '7': "/static/7.png",
        '8': "/static/8.png",
    }
    urlimg = switcher.get(arg, lambda: "invalid")
    return urlimg

def likeWithJsonData(request):
    arg = request.GET.get('q')
    num = int(arg)
    try:
        message = posts.objects.filter(num = num)
        message.like += 1
        peeplike = message.likedBy.split("-")
        message.likedBy = message.likedBy + "-" + request.user.username
        message.save()

        url_tujuan = "https://c19-simulation2.herokuapp.com/feedback/data/?format=json"
        data = requests.get(url_tujuan)
        data_JSON = json.loads(data.content)
        data_fix = data_JSON[num-1]
        return JsonResponse(data_fix, safe ="false")
    except :
        if not request.user.is_authenticated:
            return JsonResponse({}, safe = "false")
        else :
            print(posts.objects.get(pesan="b").num)
            url_tujuan = "https://c19-simulation2.herokuapp.com/feedback/data/?format=json"
            data = requests.get(url_tujuan)
            data_JSON = json.loads(data.content)
            data_fix = data_JSON[num-1]
            return JsonResponse(data_fix, safe ="false")



def dislikeWithJsonData(request):
    arg = request.GET.get('q')
    num = int(arg)
    try:
        message = posts.objects.filter(num = num)
        message.like -= 1
        peeplike = message.likedBy.split("-")
        peeplike.remove(request.user.username)
        message.likedBy = ""
        for peep in peeplike:
            message.likedBy += peep + "-"
        message.save()


        url_tujuan = "https://c19-simulation2.herokuapp.com/feedback/data/?format=json"
        data = requests.get(url_tujuan)
        data_JSON = json.loads(data.content)
        data_fix = data_JSON[num-1]
        return JsonResponse(data_fix, safe ="false")
    except :
        if not request.user.is_authenticated:
            return JsonResponse({}, safe = "false")
        else :
            url_tujuan = "https://c19-simulation2.herokuapp.com/feedback/data/?format=json"
            data = requests.get(url_tujuan)
            data_JSON = json.loads(data.content)
            data_fix = data_JSON[num-1]
            return JsonResponse(data_fix, safe ="false")

class feedbackList(APIView):

    def get(self,request):
        queryset = posts.objects.all()
        serializer = feedbackSerializer(queryset,many=True)
        return Response(serializer.data)

    def post(self):
        pass



