from django import forms
from . import models


class input_pesan(forms.ModelForm):
    class Meta:
        model = models.posts
        fields = ['pesan']
    error_messages = {
		'required' : 'Please fill'
	}
    input_attrs1 = {
		'rows' : 7,
        'class' : 'form-control textp',
        'placeholder' : 'Masukkan pesanmu disini..'

	}
    pesan = forms.CharField(label='', max_length=99999, widget=forms.Textarea(attrs=input_attrs1))
